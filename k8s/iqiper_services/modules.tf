module "oreille" {
  source = "./modules/oreille"
  namespace = var.namespace
  keycloak_host = var.keycloak_host
  keycloak_port = var.keycloak_port
  replicas = 3
}

module "bouche" {
  source = "./modules/bouche"
  namespace = var.namespace
  ready = module.oreille.done
  keycloak_host = var.keycloak_host
  keycloak_port = var.keycloak_port
  replicas = 2
}

module "oeil" {
  source = "./modules/oeil"
  namespace = var.namespace
  ready = module.oreille.done
  master_count = 1
  slave_count = 1
}
