variable "keycloak_host" {
  type = string
}

variable "keycloak_port" {
  type = number
}

variable "namespace" {
  type = string
}
