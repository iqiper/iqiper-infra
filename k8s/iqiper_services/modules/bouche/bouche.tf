resource "helm_release" "bouche-master" {
  name       = "bouche-master"
  repository = "${path.module}/../../../../charts"
  chart      = "iqiper-bouche"

  values = [
    "${file("${path.module}/helm_values/bouche.yml")}"
  ]

  namespace = var.namespace


  set {
	  name = "config.master"
	  value = true
  }

  set {
	  name = "config.slave"
	  value = false
  }

  set {
	  name = "nameOverride"
	  value = "bouche-master"
  }

  depends_on = [
	var.ready
	]
}

resource "helm_release" "bouche-slave" {
  name       = "bouche-slave"
  repository = "${path.module}/../../../../charts"
  chart      = "iqiper-bouche"

  values = [
    "${file("${path.module}/helm_values/bouche.yml")}"
  ]

  namespace = var.namespace

  set {
	  name = "replicaCount"
	  value = var.replicas
  }

  set {
	  name = "config.master"
	  value = false
  }

  set {
	  name = "config.slave"
	  value = true
  }
  
  set {
	  name = "nameOverride"
	  value = "bouche-slave"
  }

  depends_on = [
	var.ready
	]
}


output "done" {
  value = [
	  helm_release.bouche-master,
	  helm_release.bouche-slave
	]
}
