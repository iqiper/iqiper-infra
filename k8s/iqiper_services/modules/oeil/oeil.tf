resource "helm_release" "oeil-master" {
  name       = "oeil-master"
  repository = "${path.module}/../../../../charts"
  chart      = "iqiper-oeil"

  values = [
    "${file("${path.module}/helm_values/oeil.yml")}"
  ]

  namespace = var.namespace

  set {
	  name = "replicaCount"
	  value = var.master_count
  }

  set {
	  name = "config.master"
	  value = true
  }

  set {
	  name = "config.slave"
	  value = false
  }

  set {
	  name = "nameOverride"
	  value = "oeil-master"
  }

  depends_on = [
	  var.ready
	  ]
}

resource "helm_release" "oeil-slave" {
  name       = "oeil-slave"
  repository = "${path.module}/../../../../charts"
  chart      = "iqiper-oeil"

  values = [
    "${file("${path.module}/helm_values/oeil.yml")}"
  ]

  namespace = var.namespace

  set {
	  name = "replicaCount"
	  value = var.slave_count
  }

  set {
	  name = "config.master"
	  value = false
  }

  set {
	  name = "config.slave"
	  value = true
  }

  set {
	  name = "nameOverride"
	  value = "oeil-slave"
  }

  depends_on = [
	  var.ready
	  ]
}

output "done" {
  value = [
	  helm_release.oeil-master,
	  helm_release.oeil-slave
	 ]
}

