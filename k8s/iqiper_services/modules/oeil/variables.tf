variable "namespace" {
  type = string
}

variable "master_count" {
  type = number
  default = 1
}

variable "slave_count" {
  type = number
  default = 2
}

variable "ready" {
  type = any
}
