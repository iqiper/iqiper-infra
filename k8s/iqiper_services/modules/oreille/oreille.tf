resource "helm_release" "oreille" {
  name       = "oreille"
  repository = "${path.module}/../../../../charts"
  chart      = "iqiper-oreille"

  values = [
    "${file("${path.module}/helm_values/oreille.yml")}"
  ]

  set {
	  name = "replicaCount"
	  value = var.replicas
  }

  namespace = var.namespace
  depends_on = [
	  ]
}

output "done" {
  value = [helm_release.oreille]
}
