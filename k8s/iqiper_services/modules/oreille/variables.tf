variable "keycloak_host" {
  type = string
}

variable "keycloak_port" {
  type = number
}

variable "namespace" {
  type = string
}

variable "replicas" {
  type = number
  default = 1
}

