#!/bin/bash
NUMCPU=$(($(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}') * 2 / 4 ))

info() { printf "[INFO] (%s) %s\n" "$( date --iso-8601=ns )" "$*" >&2; }
error() { printf "[ERROR] (%s) %s\n" "$( date --iso-8601=ns )" "$*" >&2; }
info "Checking minikube"
STEP1="0"
STEP2="0"
STEP3="0"
MODE="apply"
minikube status >/dev/null
if [ $? -ne 0 ];
then
	info "Starting minikube"
	minikube start --cni calico --addons registry --disk-size=50G --driver=kvm2 --cpus=8 --memory 12G
fi
info "Minikube started"
trap 'error Interrupted; exit 2' INT TERM
while (( "$#" )); do
  case "$1" in
    -1|--step1)
    	STEP1="1"
    	shift
    	;;
    -2|--step2)
    	STEP2="1"
    	shift
    	;;
    -3|--step3)
    	STEP3="1"
    	shift
    	;;
    -d|--destroy)
    	MODE="destroy"
    	shift
    	;;
    -*|--*=)
    	error "Error: Unsupported flag $1" >&2
    	exit 1
    	;;
    *)
    	PARAMS="$PARAMS $1"
    	shift
    	;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

if [ $STEP1 == "1" ];
then
	info "Applying step 1"
	pushd base_cluster
	terraform workspace new dev
	terraform workspace select dev
	terraform $MODE --var-file=dev.tfvars --auto-approve
	popd
else
	info "Skipping step 1"
fi

if [[ $STEP2 == "1" && $MODE != "destroy" ]];
then
	info "Applying step 2"
	pushd cluster_config
	kubectl port-forward -n iqiper service/keycloak-http 7080:80 &
	kubectl port-forward -n prom service/prometheus-grafana 7081:80 &
	terraform workspace new dev
	terraform workspace select dev
	terraform $MODE \
		--var-file="dev.tfvars" \
		--var keycloak_user=$(terraform output --state ../base_cluster/terraform.tfstate.d/dev/terraform.tfstate keycloak-master-user | tail -c +2 | head -c -2) \
		--var keycloak_password=$(terraform output --state ../base_cluster/terraform.tfstate.d/dev/terraform.tfstate keycloak-master-password | tail -c +2 | head -c -2) \
		--auto-approve
	kill $(jobs -p)
	popd
else
	info "Skipping step 2"
fi

if [[ $STEP3 == "1" ]];
then
	info "Applying step 3"
	pushd iqiper_services
	terraform workspace new dev
	terraform workspace select dev
	terraform $MODE \
	--var-file=dev.tfvars \
	--auto-approve
	popd
else
	info "Skipping step 3"
fi

