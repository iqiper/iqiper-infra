# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.9.4"
  constraints = "1.9.4"
  hashes = [
    "h1:i/U6AFQ0carwoKyZTyePBMnz11UMZSE+7vgSPbRnqpA=",
    "zh:4cde6a0dacb2046e501b906c67deafaf424162b7ffa827d3003d62ee1d815169",
    "zh:4fe3a53b0bb96c0e78b98118797528afbdc3e7b1a3583eb9d239f2b3a9b38692",
    "zh:5e6b9d915f9c3c92ff49b3bd9892a2d7007296132b9a713c8d08f20752f25d05",
    "zh:95df66eb37f0cfd0dd5d5425be34be969c426fa7b143c6b197177b2113868ef2",
    "zh:9f494b9fc0466f97d9a4bbc63c8946560114a682b881041d17168fc9b22d2ba6",
    "zh:b8d52467766c20902ae0f6e0e93db32b72f7919d69d54d72bdb77920223b3e30",
    "zh:bbe6f23a3596a5c7a8c0d61f77d45003ab7e83776ceb4f74bc9216914c38dfcc",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "1.3.0"
  constraints = "1.3.0"
  hashes = [
    "h1:rfAi7KGnCR6LyGqEQQa7wu8zORcFkSSjxUUggE/BBmM=",
    "zh:0ca70bc0e5d3ba562f18d0f2d985749cfeb0647da34323eb18a7e2767784f1c8",
    "zh:2ea99c03838d9e4c17d180d7c07fd6b95f43fe766b00f86ccdf9e569e46eea4a",
    "zh:3e2ad7ff09343cee6d8f44f97c0c959d33c101895781b10a364b0ea3217890e7",
    "zh:405e87da32fbdced11d75673d139ab41242ee51ba888f90fd43b6ffae4f0656d",
    "zh:7121d11a65a6a73be6f93917c396d9ed383e2abe31a5f9aa78e4c58eb33430d4",
    "zh:7be67e8122953e34e3ece4e56a985792573efcf601dc83b141a24afc983d2993",
    "zh:b4e2495178895b051cffc73784dcb3ba4094e996ddad95c997ab060b4f4281da",
    "zh:d6f54e5dcc2b1e2735c211d7479e9ae5248c44ad8cf845190f7e19a6d04220ea",
    "zh:e3e45aeb6e883ad2a40fa64b966004a42c6c960b2951d459b7abf6040d44759b",
    "zh:f8004345e095422aca3f88c28bd5f37f4ab2e826aaaefa873073cc5b1ea7a020",
  ]
}

provider "registry.terraform.io/hashicorp/http" {
  version     = "1.2.0"
  constraints = "1.2.0"
  hashes = [
    "h1:g5/qN61yBHRqpGVm08BnBp1O3vVf1eIpMglYvsY13XE=",
    "zh:12df09a69dbf8219d9544466928abc65cc8cf38a26258a7586921e1e791a89a2",
    "zh:3a1a57be455011ae09df92939a2e4ea1ac1d5fd74b4617889b10a03e83b0238d",
    "zh:48f380c5e9c3abb69b7ecc40ed68f24c9ad42727d58ad530cc61f2d0524d2664",
    "zh:4d3102f731915609dd5dda55d1525c3ac3be20fae37feba2f0e2770c6e71c5a3",
    "zh:5b8c1bbe08b9f0e5f9c0d498c4f0316807595a6d55b8c194fa36d692e66f8afe",
    "zh:8d3a13bbf852a884905dce915665d8ee817fd436d088226ed4fabbc6db57d0fe",
    "zh:9025dfd565785c9424243bf78546c9c1ff3064b732ad178d9142831f5c5bea66",
    "zh:a370ac52da85934af38b60858be3e91f44a6a454b007f607b1b8a0ea307e2d27",
    "zh:ab9a1be3a3c0c760d33d0a7234909f92282f704c8145443089abbf0fddda2d85",
    "zh:d97ca45677d77eb49864887077e8457f518bd789f0609a19796b2fce70d1aadd",
    "zh:ec2f2762eeabbd2706b26f1cb03aea5301f32468147d81cc84c823ef927bc292",
    "zh:ef6851bde213957d5331039d9dc88c89d2d3182793387dae15419343c853664a",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "1.13.2"
  constraints = "1.13.2"
  hashes = [
    "h1:o4atXp0cNCmGVPr65OeRf83CImOp2gvKD7/88b83M1o=",
    "zh:011e513123e8834b681a7252e6ee0cf871e15abf0b4053da614478b8efaf2930",
    "zh:044ebfe9dca12cf7e67eae736ff88f655a103826dcfed01f1bc8f2bb75cc36b8",
    "zh:1e7ddfcc89d90f8f5b6b22531b3fb1d4b88cbf1c766fc15f447502e17ca1d6d5",
    "zh:46011d2d886617ff1f57386f0f6d0e55fccdb3b7098fc6e013b0a16b0e600a38",
    "zh:501f3a698aa8d4d79f5f54221d82433c1b25ff5a9768a93383a15209557da60c",
    "zh:5c459678149e8a348dd3b5a972c72ac5dadc19593a97286cb3ccfa4b5a4a171e",
    "zh:877a906bb8d93c3fb9c882e5b7f021aac9d30e457c4cf6057a03e275cab6f5d8",
    "zh:98abd7b2f73cd5f56bc1060575247f8147ddaf77ad9b682f68c928aeae77fcd9",
    "zh:d699b36e78f5e0604aab3595f2abb811da9c5ce3b42d14de96279021701a7805",
    "zh:dcde7a52c67954e43fb045c3736d2ae4043813c658cc6763e8e8cdef5ed544cb",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.0.0"
  hashes = [
    "h1:grDzxfnOdFXi90FRIIwP/ZrCzirJ/SfsGBe6cE0Shg4=",
    "zh:0fcb00ff8b87dcac1b0ee10831e47e0203a6c46aafd76cb140ba2bab81f02c6b",
    "zh:123c984c0e04bad910c421028d18aa2ca4af25a153264aef747521f4e7c36a17",
    "zh:287443bc6fd7fa9a4341dec235589293cbcc6e467a042ae225fd5d161e4e68dc",
    "zh:2c1be5596dd3cca4859466885eaedf0345c8e7628503872610629e275d71b0d2",
    "zh:684a2ef6f415287944a3d966c4c8cee82c20e393e096e2f7cdcb4b2528407f6b",
    "zh:7625ccbc6ff17c2d5360ff2af7f9261c3f213765642dcd84e84ae02a3768fd51",
    "zh:9a60811ab9e6a5bfa6352fbb943bb530acb6198282a49373283a8fa3aa2b43fc",
    "zh:c73e0eaeea6c65b1cf5098b101d51a2789b054201ce7986a6d206a9e2dacaefd",
    "zh:e8f9ed41ac83dbe407de9f0206ef1148204a0d51ba240318af801ffb3ee5f578",
    "zh:fbdd0684e62563d3ac33425b0ac9439d543a3942465f4b26582bcfabcb149515",
  ]
}
