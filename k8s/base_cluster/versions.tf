terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.2"
    }

	helm = {
      source = "hashicorp/helm"
      version = "1.3.0"
    }

	http = {
      source = "hashicorp/http"
      version = "1.2.0"
    }

	kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.9.4"
    }
  }
}

provider "helm" {
	kubernetes {
  		config_context =terraform.workspace == "prod" ? "iqiper-prod" : "minikube"
	}
}

provider "kubernetes" {
  config_context = terraform.workspace == "prod" ? "iqiper-prod" : "minikube"
}

provider "kubectl" {
  config_context = terraform.workspace == "prod" ? "iqiper-prod" : "minikube"
}
