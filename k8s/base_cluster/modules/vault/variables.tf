variable enabled {
	type = bool
}

variable namespace {
	type = string
}

variable ready {
	type = any
}
