resource "kubectl_manifest" "mtls-vault-ca" {
  count = var.enabled ? 1 : 0
  yaml_body = file("${path.module}/manifests/mTLS_vault_ca.yaml")
  depends_on = [var.ready]
}

resource "kubectl_manifest" "mtls-vault-issuer" {
  count = var.enabled ? 1 : 0
    yaml_body = file("${path.module}/manifests/mTLS_vault_issuer.yaml")
	depends_on = [
		kubectl_manifest.mtls-vault-ca
	]
}

resource "kubectl_manifest" "mtls-vault-tls" {
  count = var.enabled ? 1 : 0
    yaml_body = file("${path.module}/manifests/mTLS_vault_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-vault-issuer
	]
}

resource "helm_release" "vault" {
  count = var.enabled ? 1 : 0
  name       = "vault"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"

  values = [
    "${file("${path.module}/helm_values/vault.yml")}"
  ]
  namespace = var.namespace

  depends_on = [
	  kubectl_manifest.mtls-vault-tls
  ]
}

output "done" {
  value = [kubectl_manifest.mtls-vault-tls]
}
