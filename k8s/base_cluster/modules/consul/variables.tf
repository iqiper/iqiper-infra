variable consul_gossip_key {
  type = string
}

variable enabled {
	type = bool
}

variable namespace {
	type = string
}

variable ready {
	type = any
}
