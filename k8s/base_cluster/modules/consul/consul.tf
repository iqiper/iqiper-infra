resource "kubernetes_secret" "consul-gossip" {
  count = var.enabled ? 1 : 0
  metadata {
    name = "consul-gossip"
	namespace = var.namespace
  }

  data = {
    gossip_key = var.consul_gossip_key
  }

  type = "Secret"
  depends_on = [var.ready]
}

resource "helm_release" "consul" {
  count = var.enabled ? 1 : 0
  name       = "consul"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "consul"

  values = [
    "${file("${path.module}/helm_values/consul.yml")}"
  ]
  namespace = var.namespace
  depends_on = [
	  kubernetes_secret.consul-gossip,
	  kubectl_manifest.mtls-consul-issuer
  ]
}

resource "kubectl_manifest" "mtls-consul-ca" {
  count = var.enabled ? 1 : 0
  yaml_body = file("${path.module}/manifests/mTLS_consul_ca.yaml")
  depends_on = [var.ready]
}

resource "kubectl_manifest" "mtls-consul-issuer" {
  count = var.enabled ? 1 : 0
  yaml_body = file("${path.module}/manifests/mTLS_consul_issuer.yaml")
  depends_on = [
  	kubectl_manifest.mtls-consul-ca
  ]
}

output "done" {
  value = [helm_release.consul]
}
