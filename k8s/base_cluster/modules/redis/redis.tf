resource "random_password" "redis-password" {
  length = 100
  special = false
  depends_on = [var.ready]
}

locals {
	redis-password = terraform.workspace == "prod" ? random_password.redis-password.result : "redis-dev-password"
}

resource "kubernetes_secret" "redis-password" {
  metadata {
    name = "redis-password"
	namespace = var.namespace
  }

  data = {
    password = local.redis-password
  }

  type = "Secret"
}

resource "kubectl_manifest" "mtls-redis-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_redis_tls.yaml")
	depends_on = [
		var.ready
	]
}

resource "helm_release" "redis" {
  name       = "redis"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"

  values = terraform.workspace == "prod" ? [
    "${file("${path.module}/helm_values/redis.yml")}",
	"${file("${path.module}/helm_values/redis.prod.yml")}"
  ] : [
    "${file("${path.module}/helm_values/redis.yml")}"
  ]
  namespace = var.namespace
  depends_on = [kubectl_manifest.mtls-redis-tls]
}

output "done" {
  value = [helm_release.redis]
}

output "redis-password" {
  sensitive = true
  value = local.redis-password
}

