resource "helm_release" "prom" {
  name       = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"

  namespace = var.namespace
  depends_on = [
	  var.ready
  ]
}

output "done" {
  value = [helm_release.prom]
}
