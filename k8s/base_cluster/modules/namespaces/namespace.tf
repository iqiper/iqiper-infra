resource "kubernetes_namespace" "consul" {
  metadata {
    annotations = {
      name = "consul"
    }

    name = "consul"
  }
}

resource "kubernetes_namespace" "cert-manager" {
  metadata {
    annotations = {
      name = "cert-manager"
    }

    name = "cert-manager"
  }
}

resource "kubernetes_namespace" "projectcontour" {
  metadata {
    annotations = {
      name = "projectcontour"
    }

    name = "projectcontour"
  }
}

resource "kubernetes_namespace" "iqiper" {
  metadata {
    annotations = {
      name = "iqiper"
    }

    name = "iqiper"
  }
}

resource "kubernetes_namespace" "prom" {
  metadata {
    annotations = {
      name = "prom"
    }

    name = "prom"
  }
}

output consul {
	value = kubernetes_namespace.consul
}

output cert-manager {
	value = kubernetes_namespace.cert-manager
}

output projectcontour {
	value = kubernetes_namespace.projectcontour
}

output iqiper {
	value = kubernetes_namespace.iqiper
}

output prom {
	value = kubernetes_namespace.prom
}

output done {
	value = [
		kubernetes_namespace.consul,
		kubernetes_namespace.cert-manager,
		kubernetes_namespace.projectcontour,
		kubernetes_namespace.iqiper,
		kubernetes_namespace.prom
		]
}
