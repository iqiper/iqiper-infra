resource "kubectl_manifest" "mtls-contour-ca" {
    yaml_body = file("${path.module}/manifests/mTLS_contour_ca.yaml")
	depends_on = [var.ready]
}

resource "kubectl_manifest" "mtls-contour-issuer" {
    yaml_body = file("${path.module}/manifests/mTLS_contour_issuer.yaml")
	depends_on = [
		kubectl_manifest.mtls-contour-ca
	]
}

resource "kubectl_manifest" "mtls-contour-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_contour_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-contour-issuer
	]
}

resource "kubectl_manifest" "mtls-envoy-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_envoy_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-contour-issuer
	]
}

resource "kubectl_manifest" "mtls-ingress-ca" {
    yaml_body = file("${path.module}/manifests/mTLS_contour_ingress_ca.yaml")
	depends_on = [
		kubectl_manifest.mtls-contour-ca
	]
}

resource "kubectl_manifest" "mtls-ingress-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_contour_ingress_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-ingress-ca
	]
}

resource "helm_release" "contour" {
  name       = "contour"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "contour"
  wait = false # TODO Check with upstream

  values = [
    "${file("${path.module}/helm_values/contour.yml")}"
  ]
  namespace = var.namespace
  depends_on = [kubectl_manifest.mtls-envoy-tls]
}

output "done" {
  value = [helm_release.contour]
}
