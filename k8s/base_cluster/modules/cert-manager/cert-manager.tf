resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.1.0"

  namespace = var.namespace
  values = [
    "${file("${path.module}/helm_values/cert-manager.yml")}"
  ]
  depends_on = [
	  var.ready
  ]
}

resource "kubectl_manifest" "mtls-0-issuer" {
    yaml_body = file("${path.module}/manifests/mTLS_0_issuer.yaml")
	depends_on = [
		helm_release.cert_manager
	]
}

resource "kubectl_manifest" "mtls-iqiper-ca" {
    yaml_body = file("${path.module}/manifests/mTLS_iqiper_ca.yaml")
	depends_on = [
		kubectl_manifest.mtls-0-issuer
	]
}

resource "kubectl_manifest" "mtls-iqiper-issuer" {
    yaml_body = file("${path.module}/manifests/mTLS_iqiper_issuer.yaml")
	depends_on = [
		kubectl_manifest.mtls-iqiper-ca
	]
}


output "done" {
  value = true
  depends_on = [
	  kubectl_manifest.mtls-0-issuer,
	  kubectl_manifest.mtls-iqiper-ca,
	  kubectl_manifest.mtls-iqiper-issuer,
  ]
}
