resource "helm_release" "mailhog" {
  name       = "mailhog"
  repository = "https://codecentric.github.io/helm-charts"
  chart      = "mailhog"
  version    = "3.3.0"

  namespace = var.namespace
  values = [
    "${file("${path.module}/helm_values/mailhog.yml")}"
  ]
  depends_on = [
	  var.ready
  ]
}

output "done" {
  value = true
  depends_on = [
	  helm_release.mailhog
  ]
}
