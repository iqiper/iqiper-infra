#!/bin/bash -eux
psql -U postgres -c "CREATE ROLE keycloak CREATEDB LOGIN NOCREATEROLE NOSUPERUSER"
psql -U postgres -c "CREATE ROLE iqiper CREATEDB LOGIN NOCREATEROLE NOSUPERUSER"
psql -U postgres -c "CREATE DATABASE keycloak OWNER = keycloak ENCODING = \"UTF-8\""
psql -U postgres -c "CREATE DATABASE iqiper OWNER = iqiper ENCODING = \"UTF-8\""
psql -U postgres -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";" -d keycloak

psql -U postgres -c "CREATE SCHEMA IF NOT EXISTS AUTHORIZATION iqiper;" -d iqiper
psql -U postgres -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\" WITH SCHEMA iqiper;" -d iqiper

psql -U postgres -c "revoke all on database postgres from PUBLIC;" -d iqiper
psql -U postgres -c "revoke all on schema public from PUBLIC;" -d iqiper
psql -U postgres -c "revoke all on all tables in schema public from PUBLIC;" -d iqiper
