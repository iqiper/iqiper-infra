resource "random_password" "postgres-admin-password" {
  length = 100
  special = false
}

resource "random_password" "postgres-repmgr-password" {
  length = 100
  special = false
}

resource "random_password" "pgpool-admin-password" {
  length = 100
  special = false
}

# resource "random_password" "keycloak-cert-store-password" {
#   length = 64
#   special = false
# }

resource "kubectl_manifest" "mtls-postgres-ca" {
    yaml_body = file("${path.module}/manifests/mTLS_postgres_ca.yaml")
	depends_on = [var.ready]
}

resource "kubectl_manifest" "mtls-postgres-issuer" {
    yaml_body = file("${path.module}/manifests/mTLS_postgres_issuer.yaml")
	depends_on = [
		kubectl_manifest.mtls-postgres-ca
	]
}

resource "kubernetes_secret" "mtls-postgres-keycloak-tls-secret-password" {
  metadata {
    name = "mtls-postgres-keycloak-tls-secret-password"
	namespace = var.namespace
  }
  data = {
    password = "nopassword"
  }
  type = "Secret"
}

resource "kubectl_manifest" "mtls-keycloak-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_keycloak_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-postgres-issuer
	]
}

resource "kubectl_manifest" "mtls-oreille-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_oreille_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-postgres-issuer
	]
}

resource "kubectl_manifest" "mtls-bouche-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_bouche_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-postgres-issuer
	]
}

resource "kubectl_manifest" "mtls-oeil-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_oeil_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-postgres-issuer
	]
}

resource "kubectl_manifest" "mtls-postgres-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_postgres_tls.yaml")
	depends_on = [
		kubectl_manifest.mtls-postgres-issuer
	]
}


resource "kubernetes_config_map" "init-postgres" {
  metadata {
    name					= "init-postgres"
	namespace				= var.namespace
  }

  data = {
    "init.sh" = file("${path.module}/config/init.sh")
  }
}

resource "helm_release" "postgres" {
  name       = "postgresql-ha"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql-ha"

  values = [
    "${file("${path.module}/helm_values/postgres.yml")}"
  ]

  set {
    name  = "postgresql.repmgrUsername"
    value = "repmngr"
  }
  set {
    name  = "postgresql.repmgrPassword"
    value = random_password.postgres-repmgr-password.result
  }

  set {
    name  = "postgresql.username"
    value = "postgres"
  }

  set {
    name  = "postgresql.password"
    value = random_password.postgres-admin-password.result
  }

  set {
    name  = "pgpool.adminPassword"
    value = random_password.pgpool-admin-password.result
  }
  set {
    name  = "pgpool.adminUsername"
    value = "adminuser"
  }
  set {
    name  = "postgresql.initdbScriptsCM"
    value = kubernetes_config_map.init-postgres.metadata.0.name
  }
  namespace = var.namespace
  depends_on = [
	  kubectl_manifest.mtls-postgres-issuer,
	  kubernetes_config_map.init-postgres,
	  kubernetes_secret.mtls-postgres-keycloak-tls-secret-password
  ]
}

output "done" {
  value = [helm_release.postgres]
}

# resource "kubernetes_config_map" "postgres-config" {
#   metadata {
#     name = "postgres"
# 	namespace = var.namespace
#   }

#   data = {
#     "postgres.yaml" = file("${path.module}/config/contour.yml")
#   }
#   depends_on = [
# 	  helm_release.contour
#   ]
# }

# FIXME, Only for dev

