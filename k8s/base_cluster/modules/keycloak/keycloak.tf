
resource "random_password" "keycloak-master-user" {
  length = 16
  special = false
}

resource "random_password" "keycloak-master-password" {
  length = 100
  special = true
}

resource "kubernetes_secret" "keycloak-master-creds" {
  metadata {
    name = "keycloak-master-creds"
	namespace = var.namespace
  }

  data = {
    username = random_password.keycloak-master-user.result
    password = random_password.keycloak-master-password.result
  }

  type = "kubernetes.io/basic-auth"
}

resource "helm_release" "keycloak" {
  name       = "keycloak"
  repository = "https://codecentric.github.io/helm-charts"
  chart      = "keycloak"

  values = [
    "${file("${path.module}/helm_values/keycloak.yml")}"
  ]
  namespace = var.namespace
  depends_on = [
	  var.ready,
	  kubernetes_secret.keycloak-master-creds
	  ]
}

resource "kubectl_manifest" "mtls-keycloak-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_keycloak_tls.yaml")
	depends_on = [
		var.ready
	]
}

output "done" {
  value = [helm_release.keycloak]
}

output "keycloak-master-user" {
  value = random_password.keycloak-master-user
}

output "keycloak-master-password" {
  sensitive = true
  value = random_password.keycloak-master-password
}
