variable namespace {
	type = string
}

variable gitlab_token {
	type = string
}

variable ready {
	type = any
}
