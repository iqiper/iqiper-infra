variable username {
	type = string
}

variable token {
	type = string
}

variable namespace {
	type = string
}

variable ready {
	type = any
}
