resource "kubernetes_secret" "gitlab_pull_token" {
  metadata {
    name = "gitlab-pull-token"
	namespace = var.namespace
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "registry.gitlab.com": {
      "auth": "${base64encode("${var.username}:${var.token}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"
  depends_on = [var.ready]
}

resource "kubernetes_secret" "gitlab_token" {
  metadata {
    name = "gitlab-token"
	namespace = var.namespace
  }

  data = {
    "username" = var.username
	"token" = var.token
  }

  type = "Opaque"
  depends_on = [var.ready]
} 

output "done" {
  value = [kubernetes_secret.gitlab_pull_token]
}
