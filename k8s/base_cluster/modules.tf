module "namespaces" {
  source = "./modules/namespaces"
}

module "gitlab_pull_token" {
  source = "./modules/gitlab_pull_token"
  namespace = module.namespaces.iqiper.metadata.0.name
  username = var.gitlab_username
  token = var.gitlab_token
  ready = module.namespaces.done
}
module "cert-manager" {
  source = "./modules/cert-manager"
  namespace = module.namespaces.cert-manager.metadata.0.name
  ready = module.namespaces.done
}

module "consul" {
  source = "./modules/consul"
  enabled = var.enable_consul
  consul_gossip_key = var.consul_gossip_key
  namespace = module.namespaces.consul.metadata.0.name
  ready = module.cert-manager.done
}

module "prometheus-operator" {
  source = "./modules/prometheus-operator"
  ready = module.cert-manager.done
  namespace = module.namespaces.prom.metadata.0.name
}

module "vault" {
  source = "./modules/vault"
  enabled = var.enable_vault
  namespace = module.namespaces.iqiper.metadata.0.name
  ready = module.cert-manager.done
}

module "contour" {
	source = "./modules/contour"
	namespace = module.namespaces.projectcontour.metadata.0.name
	ready = module.cert-manager.done
}

module "postgres" {
	source = "./modules/postgres"
	namespace = module.namespaces.iqiper.metadata.0.name
	ready = module.prometheus-operator.done
}

module "redis" {
	source = "./modules/redis"
	namespace = module.namespaces.iqiper.metadata.0.name
	ready = module.prometheus-operator.done
}

module "keycloak" {
	source = "./modules/keycloak"
	namespace = module.namespaces.iqiper.metadata.0.name
	ready = module.postgres.done
	gitlab_token = var.gitlab_token
}

module "mailhog" {
	source = "./modules/mailhog"
	namespace = module.namespaces.iqiper.metadata.0.name
	ready = module.namespaces.done
}

output "redis-password" {
  sensitive = true
  value = module.redis.redis-password
}

output "keycloak-master-user" {
	value = module.keycloak.keycloak-master-user.result
}

output "keycloak-master-password" {
	sensitive = true
	value = module.keycloak.keycloak-master-password.result
}
