variable "consul_gossip_key" {
  type = string
}

variable "enable_consul" {
	type = bool
}

variable "enable_vault" {
	type = bool
}

variable "gitlab_username" {
  type = string
}

variable "gitlab_token" {
  type = string
}
