resource "grafana_organization" "iqiper" {
  name         = "iqiper"
  admin_user   = "admin"
  create_users = false
}

resource "grafana_team" "iqiper" {
  name = "iqiper"
}

resource "grafana_team_preferences" "team_preferences" {
  team_id           = grafana_team.iqiper.id
  theme             = "dark"
  timezone          = "browser"
}
