resource "grafana_dashboard" "postgres" {
  config_json = "${file("${path.module}/dashboards/postgres.json")}"
}

resource "grafana_dashboard" "redis" {
  config_json = "${file("${path.module}/dashboards/redis.json")}"
}

resource "grafana_dashboard" "oeil" {
  config_json = "${file("${path.module}/dashboards/oeil.json")}"
}

resource "grafana_dashboard" "bouche" {
  config_json = "${file("${path.module}/dashboards/bouche.json")}"
}

resource "grafana_dashboard" "oreille" {
  config_json = "${file("${path.module}/dashboards/oreille.json")}"
}
