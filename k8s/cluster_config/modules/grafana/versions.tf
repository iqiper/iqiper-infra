terraform {
  required_providers {
    grafana = {
      source = "grafana/grafana"
      version = "1.7.0"
    }
  }
}

provider "grafana" {
  url = var.url
  auth = "${var.username}:${var.password}"
}
