variable url {
	type = string
}

variable username {
	type = string
}

variable password {
	type = string
}
