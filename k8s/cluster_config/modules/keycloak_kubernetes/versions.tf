terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.2"
    }
  }
}

provider "kubernetes" {
  config_context = terraform.workspace == "prod" ? "iqiper-prod" : "minikube"
}
