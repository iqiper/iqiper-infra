resource "kubernetes_secret" "oidc-iqiper-creds" {
  metadata {
    name = "oidc-iqiper-creds"
	namespace = "iqiper"
  }

  data = {
    "client_id" = var.client-id
	"client_secret" = var.client-secret
  }

  type = "Opaque"
}
