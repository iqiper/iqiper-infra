locals {
  scope_csv = csvdecode(file("${path.module}/scopes.csv"))
}
resource "keycloak_openid_client_scope" "iqiper_io_scopes" {
  for_each = { for sc in local.scope_csv : sc.name => sc }
  realm_id    = keycloak_realm.iqiper.id
  name        = each.value.scope
  description = each.value.description
}

resource "keycloak_openid_client_optional_scopes" "iqiper_io_optional_scopes" {
    realm_id		= keycloak_realm.iqiper.id
    client_id		= keycloak_openid_client.iqiper.id

    optional_scopes = concat([
        "address",
        "phone",
        "offline_access",
    ], local.scope_csv[*].scope)
}
