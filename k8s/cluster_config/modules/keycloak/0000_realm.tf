resource "keycloak_realm" "iqiper" {
  realm             = "iqiper"
  enabled           = true
  display_name      = "iqiper"
  display_name_html = "<b>IQIPER</b>"

  registration_allowed           = true
  registration_email_as_username = false
  edit_username_allowed          = true
  reset_password_allowed         = true
  remember_me                    = true
  verify_email                   = false
  login_with_email_allowed       = true
  duplicate_emails_allowed       = false

  login_theme          = var.default_theme ? "keycloak" : "iqiper"
  account_theme        = var.default_theme ? "keycloak" : "iqiper"
  access_code_lifespan = "1m"
  ssl_required         = "external"

  smtp_server {
    host = "mailhog.iqiper.svc.cluster.local"
    port = "1025"
    from = "keycloak@iqiper.io"
  }
  internationalization {
    supported_locales = [
      "en",
      "fr",
    ]
    default_locale = "en"
  }

  security_defenses {
    headers {
      x_frame_options                     = "DENY"
      content_security_policy             = "frame-src 'self'; frame-ancestors 'self'; object-src 'none';"
      content_security_policy_report_only = ""
      x_content_type_options              = "nosniff"
      x_robots_tag                        = "none"
      x_xss_protection                    = "1; mode=block"
      strict_transport_security           = "max-age=31536000; includeSubDomains"
    }
    brute_force_detection {
      permanent_lockout                = false
      max_login_failures               = 30
      wait_increment_seconds           = 60
      quick_login_check_milli_seconds  = 1000
      minimum_quick_login_wait_seconds = 60
      max_failure_wait_seconds         = 900
      failure_reset_time_seconds       = 43200
    }
  }
}

