variable url {
	type = string
}

variable user {
	type = string
}

variable password {
	type = string
}

variable default_theme {
	type = bool
	default = false
}

variable access_token_lifespan {
	type = number
	default = 60
}
