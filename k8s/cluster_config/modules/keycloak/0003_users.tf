resource "keycloak_user" "user0" {
	count = terraform.workspace == "prod" ? 0 : 1
    realm_id   = keycloak_realm.iqiper.id
    username   = "user0"
    enabled    = true

    email      = "user0@example.com"
    first_name = "Bob"
    last_name  = "Bobby"
	email_verified = true	
	initial_password {
      value     = "password0"
      temporary = false
    }
}

resource "keycloak_user" "user1" {
	count = terraform.workspace == "prod" ? 0 : 1
    realm_id   = keycloak_realm.iqiper.id
    username   = "user1"
    enabled    = true
    email      = "user1@example.com"
    first_name = "Alice"
    last_name  = "Alicy"
	email_verified = true	
	initial_password {
      value     = "password1"
      temporary = false
    }
}

resource "keycloak_user" "user2" {
	count = terraform.workspace == "prod" ? 0 : 1
    realm_id   = keycloak_realm.iqiper.id
    username   = "user2"
    enabled    = true
    email      = "user2@example.com"
    first_name = "Malory"
    last_name  = "BadGuy"
	email_verified = true	
	initial_password {
      value     = "password2"
      temporary = false
    }
}
