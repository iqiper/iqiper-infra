resource "random_password" "keycloak-client-iqiper-secret" {
  length = 256
  special = false
}

resource "random_password" "keycloak-client-iqiper-id" {
  length = 64
  special = false
}

locals {
	keycloak-client-iqiper-secret = terraform.workspace == "prod" ? random_password.keycloak-client-iqiper-secret.result : "iqiper-secret"
	keycloak-client-iqiper-id = terraform.workspace == "prod" ? random_password.keycloak-client-iqiper-secret.result : "iqiper"
}

resource "keycloak_openid_client" "iqiper" {
  realm_id  = keycloak_realm.iqiper.id
  client_id = local.keycloak-client-iqiper-id
  client_secret = local.keycloak-client-iqiper-secret
  name    = "iqiper"
  enabled = true

  access_type = "CONFIDENTIAL"
  valid_redirect_uris  = terraform.workspace == "prod" ? [] : ["*"]
  standard_flow_enabled        = true
  implicit_flow_enabled        = false
  direct_access_grants_enabled = terraform.workspace == "prod" ? false : true
  service_accounts_enabled     = true
  access_token_lifespan        = var.access_token_lifespan
  consent_required             = terraform.workspace == "prod" ? true : false
}

resource "keycloak_openid_hardcoded_claim_protocol_mapper" "iqiper_nbf_hardcoded_claim" {
    realm_id    = keycloak_realm.iqiper.id
    client_id   = keycloak_openid_client.iqiper.id
    name        = "hardcoded_nbf"

    claim_name  = "nbf"
    claim_value = "0"
	claim_value_type = "long"
	add_to_id_token = true
	add_to_access_token = true
	add_to_userinfo = true
}

resource "keycloak_openid_audience_protocol_mapper" "iqiper_audience_mapper" {
    realm_id                 = keycloak_realm.iqiper.id
    client_id                = keycloak_openid_client.iqiper.id
    name                     = "iqiper-audience-mapper"

    included_client_audience = "iqiper"
	add_to_id_token = true
	add_to_access_token = true
}

data "keycloak_openid_client" "realm_management" {
  realm_id = keycloak_realm.iqiper.id
  client_id = "realm-management"
}

data "keycloak_role" "realm_management_query_users" {
  realm_id = keycloak_realm.iqiper.id
  client_id = data.keycloak_openid_client.realm_management.id
  name = "query-users"
}

data "keycloak_role" "realm_management_manage_users" {
  realm_id = keycloak_realm.iqiper.id
  client_id = data.keycloak_openid_client.realm_management.id
  name = "manage-users"
}

data "keycloak_role" "realm_management_view_users" {
  realm_id = keycloak_realm.iqiper.id
  client_id = data.keycloak_openid_client.realm_management.id
  name = "view-users"
}

resource "keycloak_openid_client_service_account_role" "iqiper-service-account-query-users-role" {
  realm_id                = keycloak_realm.iqiper.id
  service_account_user_id = keycloak_openid_client.iqiper.service_account_user_id
  client_id               = data.keycloak_openid_client.realm_management.id
  role                    = data.keycloak_role.realm_management_query_users.name
}

resource "keycloak_openid_client_service_account_role" "iqiper-service-account-manage-users-role" {
  realm_id                = keycloak_realm.iqiper.id
  service_account_user_id = keycloak_openid_client.iqiper.service_account_user_id
  client_id               = data.keycloak_openid_client.realm_management.id
  role                    = data.keycloak_role.realm_management_manage_users.name
}

resource "keycloak_openid_client_service_account_role" "iqiper-service-account-view-users-role" {
  realm_id                = keycloak_realm.iqiper.id
  service_account_user_id = keycloak_openid_client.iqiper.service_account_user_id
  client_id               = data.keycloak_openid_client.realm_management.id
  role                    = data.keycloak_role.realm_management_view_users.name
}

output "keycloak-client-iqiper-id" {
  sensitive = false
  value = local.keycloak-client-iqiper-id
}

output "keycloak-client-iqiper-secret" {
  sensitive = true
  value = local.keycloak-client-iqiper-secret
}
