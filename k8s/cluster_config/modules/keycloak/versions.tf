terraform {
  required_providers {
    keycloak = {
      source = "mrparkers/keycloak"
      version = "2.0.0-rc.0"
    }
	
	random = {
      source = "hashicorp/random"
      version = "2.3.0"
    }
  }
}

provider "keycloak" {	
    client_id     = "admin-cli"
    username      = var.user
    password      = var.password
	realm         = "master"
	tls_insecure_skip_verify = terraform.workspace == "prod" ? false : true
	url           = var.url
}

