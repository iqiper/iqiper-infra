#!/bin/bash
echo "Waiting for keycloak...."
COUNTER=240
until $(curl --output /dev/null --silent --head --fail $KEYCLOAK_URL); do
    printf '.'
    sleep 1
	COUNTER=$(($COUNTER-1))
	if [ $COUNTER -eq 0 ];
	then
		echo "KEYCLOAK TIMEOUT"
		exit 1
	fi
done

