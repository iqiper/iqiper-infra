#!/bin/bash
get_date()
{
	unameOut="$(uname -s)"
	case "${unameOut}" in
	    Linux*)     NOW_DATE=$( date --iso-8601=ns );;
	    Darwin*)    NOW_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ");;
	    *)          NOW_DATE=$( date --iso-8601=ns );;
	esac
	echo $NOW_DATE
}
info() { printf "[INFO] (%s) %s\n" "$( get_date )" "$*" >&2; }
error() { printf "[ERROR] (%s) %s\n" "$( get_date )" "$*" >&2; }
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
API_PATH=$(realpath $DIR/../../api/iqiper.yml)
API_OUT=$(mktemp)
swagger-cli bundle --type json --dereference --outfile $API_OUT $API_PATH
RES=0
for scope in $(cat $API_OUT | jq '.. | .OIDC? // empty | .[]' | tr -d \" \
	| grep -e ^iqiper\.io. | sort | uniq)
do
	info "Checking scope $scope"
	grep "$scope", < $DIR/scopes.csv >/dev/null
	if [ $? -ne 0 ]
	then
		error "Scope discrepencies : $scope"
		RES=1
	fi
done
# rm $API_OUT
exit $RES
