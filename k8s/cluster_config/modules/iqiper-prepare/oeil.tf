resource "kubernetes_config_map" "oeil-config" {
  metadata {
    name = "oeil-config"
	namespace = var.namespace
  }

  data = {
    "oeil.yml" = "${file("${path.module}/config/oeil.yml")}"
  }
}

resource "kubectl_manifest" "mtls-oeil-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_oeil_tls.yaml")
	depends_on = [
	]
}
