resource "kubernetes_config_map" "bouche-config" {
  metadata {
    name = "bouche-config"
	namespace = var.namespace
  }

  data = {
    "bouche.yml" = "${file("${path.module}/config/bouche.yml")}"
  }
}

resource "kubectl_manifest" "mtls-bouche-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_bouche_tls.yaml")
	depends_on = [
	]
}
