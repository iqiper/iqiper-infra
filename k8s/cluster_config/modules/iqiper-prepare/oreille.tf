resource "kubernetes_config_map" "oreille-config" {
  metadata {
    name = "oreille-config"
	namespace = var.namespace
  }

  data = {
    "oreille.yml" = "${file("${path.module}/config/oreille.yml")}"
  }
}

resource "kubernetes_config_map" "oreille-openapi-specs" {
  metadata {
    name = "oreille-openapi-specs"
	namespace = var.namespace
  }

  data = {
    "oreille.json" = "${file("${path.module}/../../../../api/iqiper.json")}"
  }
  depends_on = [
  ]
}

resource "kubectl_manifest" "mtls-oreille-tls" {
    yaml_body = file("${path.module}/manifests/mTLS_oreille_tls.yaml")
	depends_on = [
	]
}
