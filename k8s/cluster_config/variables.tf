variable "keycloak_url" {
  type = string
}

variable "keycloak_user" {
  type = string
}

variable "keycloak_password" {
  type = string
}

variable "keycloak_default_theme" {
  type = bool
  default = false
}

variable "keycloak_access_token_lifespan" {
  type = number
  default = 60
}

variable "grafana_url" {
  type = string
}

variable "grafana_username" {
  type = string
}

variable "grafana_password" {
  type = string
}
