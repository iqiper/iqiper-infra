module "keycloak" {
  source = "./modules/keycloak"
  url = var.keycloak_url
  user = var.keycloak_user
  password = var.keycloak_password
  access_token_lifespan = var.keycloak_access_token_lifespan
  default_theme = var.keycloak_default_theme
}

module "keycloak_kubernetes" {
  source = "./modules/keycloak_kubernetes"
  client-id = module.keycloak.keycloak-client-iqiper-id
  client-secret = module.keycloak.keycloak-client-iqiper-secret
}

module "iqiper-prepare" {
  source = "./modules/iqiper-prepare"
  namespace = "iqiper"
}

module "grafana" {
  source = "./modules/grafana"
  url = var.grafana_url
  username = var.grafana_username
  password = var.grafana_password
}


output "keycloak-client-iqiper-id" {
  sensitive = false
  value = module.keycloak.keycloak-client-iqiper-id
}

output "keycloak-client-iqiper-secret" {
  sensitive = true
  value = module.keycloak.keycloak-client-iqiper-secret
}
