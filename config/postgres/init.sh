#!/bin/bash
psql -v iqiper_password="'$IQIPER_PASSWORD'" -v keycloak_password="'$KEYCLOAK_PASSWORD'" <<EOF
CREATE ROLE iqiper WITH LOGIN PASSWORD :iqiper_password;
CREATE ROLE keycloak WITH LOGIN PASSWORD :keycloak_password;
CREATE DATABASE iqiper WITH OWNER iqiper;
CREATE DATABASE keycloak WITH OWNER keycloak;
EOF
