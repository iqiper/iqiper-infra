#!/bin/bash -xe
DIR=$(dirname "$(readlink -f "$0")")
export PKG_CONFIG_ALLOW_CROSS=1
export OPENSSL_STATIC=true
export OPENSSL_DIR=/musl
# minikube start
pushd k8s
# ./run.sh -1 -2
popd
eval $(minikube docker-env)
tilt up --hud=true
tilt down 
