#!/bin/bash
minikube start --cni calico --addons registry --disk-size=50G --driver=kvm2 --cpus=8 --memory 12G
