load('ext://restart_process', 'docker_build_with_restart')
load('ext://local_output', 'local_output')
config.define_string('kubernetes-ctx', args=False)
config.define_string('gitlab-username', args=False)
config.define_string('gitlab-token', args=False)
config.define_string('iqiper-oreille-root', args=False)
config.define_string('iqiper-oeil-root', args=False)
config.define_string('iqiper-bouche-root', args=False)
config.define_string('iqiper-infra-root', args=False)
config.define_string('registry')
cfg = config.parse()
allow_k8s_contexts(str(cfg.get('kubernertes-ctx', 'microk8s'))) # Allow the kubectl microk8s context

if cfg.get('gitlab-username', '') == '':
	fail('Gitlab username is missing')

if cfg.get('gitlab-token', '') == '':
	fail('Gitlab token is missing')

if cfg.get('iqiper-oreille-root', '') == '':
	fail('The root of the iqiper-oreille project is not set')

os.putenv('IQIPER_GITLAB_USERNAME', str(cfg.get('gitlab-username')))
os.putenv('IQIPER_GITLAB_TOKEN', str(cfg.get('gitlab-token')))
os.putenv('IQIPER_INFRA_ROOT', cfg.get('iqiper-infra-root'))
os.unsetenv('IQIPER_GITLAB_USERNAME')
os.unsetenv('IQIPER_GITLAB_TOKEN')
os.putenv('IQIPER_DATA_ROOT', '/tmp/iqiper_data')
uid = local('id -u', quiet=False) # Get the user uid
gid = local('id -g', quiet=False) # Get the user gid
registryAddress = local_output('minikube ip') # Get minikube IP
registry = registryAddress + ':5000/'
registryFromCluster = 'localhost:5000'
default_registry(registryFromCluster)
os.putenv('IQIPER_LOCAL_USER_ID', str(uid))
os.putenv('IQIPER_LOCAL_GROUP_ID', str(gid))

local_resource(
	'Build api for oreille',
	'swagger-cli bundle -r ' + cfg.get('iqiper-infra-root') + '/api/iqiper.yml > ' + cfg.get('iqiper-oreille-root') + '/.build/api.json',
	deps=[
		cfg.get('iqiper-infra-root') + '/api'
	]
)

# local_resource(
# 	'Build oreille',
# 	'cd ' + cfg.get('iqiper-oreille-root') + ' && cargo build --target x86_64-unknown-linux-musl',
# 	deps=[
# 		cfg.get('iqiper-oreille-root') + '/src'
# 	]
# )

# local_resource(
# 	'Build oeil',
# 	'cd ' + cfg.get('iqiper-oeil-root') + ' && cargo build --target x86_64-unknown-linux-musl',
# 	deps=[
# 		cfg.get('iqiper-oeil-root') + '/src'
# 	]
# )

# local_resource(
# 	'Build bouche',
# 	'cd ' + cfg.get('iqiper-bouche-root') + ' && cargo build --target x86_64-unknown-linux-musl',
# 	deps=[
# 		cfg.get('iqiper-bouche-root') + '/src'
# 	]
# )

docker_build(
  'oreille',
  cfg.get('iqiper-oreille-root'),
  dockerfile=cfg.get('iqiper-oreille-root') + '/Dockerfile.dev.oreille',
  live_update=[
	  fall_back_on(cfg.get('iqiper-oreille-root') + '/Dockerfile.dev.oreille'),
	  fall_back_on(cfg.get('iqiper-oreille-root') + '/utils/docker_entrypoint.dev.sh'),
	  sync(cfg.get('iqiper-oreille-root') + '/.build/api.json', '/config/api.json'),
	  sync(cfg.get('iqiper-oreille-root') + '/target/debug/iqiper-oreille', '/usr/src/oreille/target/debug/iqiper-oreille'),
	  sync(cfg.get('iqiper-oreille-root') + '/target/x86_64-unknown-linux-musl/debug/iqiper-oreille', '/usr/src/oreille/target/x86_64-unknown-linux-musl/debug/iqiper-oreille'),
	  run('touch /.restart')
  ],
)

docker_build(
  'oeil',
  cfg.get('iqiper-oeil-root'),
  dockerfile=cfg.get('iqiper-oeil-root') + '/Dockerfile.dev.oeil',
  live_update=[
	  fall_back_on(cfg.get('iqiper-oeil-root') + '/Dockerfile.dev.oeil'),
	  fall_back_on(cfg.get('iqiper-oeil-root') + '/utils/docker_entrypoint.dev.sh'),
	  sync(cfg.get('iqiper-oeil-root') + '/target/debug/iqiper-oeil', '/usr/src/oeil/target/debug/iqiper-oeil'),
	  sync(cfg.get('iqiper-oeil-root') + '/target/x86_64-unknown-linux-musl/debug/iqiper-oeil', '/usr/src/oeil/target/x86_64-unknown-linux-musl/debug/iqiper-oeil'),
	  run('touch /.restart')
  ],
)

docker_build(
  'bouche',
  cfg.get('iqiper-bouche-root'),
  dockerfile=cfg.get('iqiper-bouche-root') + '/Dockerfile.dev.bouche',
  live_update=[
	  fall_back_on(cfg.get('iqiper-bouche-root') + '/Dockerfile.dev.bouche'),
	  fall_back_on(cfg.get('iqiper-bouche-root') + '/utils/docker_entrypoint.dev.sh'),
	  sync(cfg.get('iqiper-bouche-root') + '/target/debug/iqiper-bouche', '/usr/src/bouche/target/debug/iqiper-bouche'),
	  sync(cfg.get('iqiper-bouche-root') + '/target/x86_64-unknown-linux-musl/debug/iqiper-bouche', '/usr/src/bouche/target/x86_64-unknown-linux-musl/debug/iqiper-bouche'),
	  run('touch /.restart')
  ],
)

k8s_yaml(helm(cfg.get('iqiper-infra-root') + '/charts/iqiper-oreille/',
	name='oreille',
	namespace='iqiper',
	values=[cfg.get('iqiper-infra-root') + '/k8s/iqiper_services/modules/oreille/helm_values/oreille.yml'],
	set=['replicaCount=1','image.repository=oreille']))

k8s_yaml(helm(cfg.get('iqiper-infra-root') + '/charts/iqiper-oeil/',
	name='oeil-master',
	namespace='iqiper',
	values=[cfg.get('iqiper-infra-root') + '/k8s/iqiper_services/modules/oeil/helm_values/oeil.yml'],
	set=['replicaCount=1', 'image.repository=oeil', 'config.master=true', 'config.slave=false']), allow_duplicates=True)

k8s_yaml(helm(cfg.get('iqiper-infra-root') + '/charts/iqiper-oeil/',
	name='oeil-slave',
	namespace='iqiper',
	values=[cfg.get('iqiper-infra-root') + '/k8s/iqiper_services/modules/oeil/helm_values/oeil.yml'],
	set=['replicaCount=1', 'image.repository=oeil', 'config.master=false', 'config.slave=true']), allow_duplicates=True)


k8s_yaml(helm(cfg.get('iqiper-infra-root') + '/charts/iqiper-bouche/',
	name='bouche-master',
	namespace='iqiper',
	values=[cfg.get('iqiper-infra-root') + '/k8s/iqiper_services/modules/bouche/helm_values/bouche.yml'],
	set=['replicaCount=1', 'image.repository=bouche', 'config.master=true', 'config.slave=false']), allow_duplicates=True)

k8s_yaml(helm(cfg.get('iqiper-infra-root') + '/charts/iqiper-bouche/',
	name='bouche-slave',
	namespace='iqiper',
	values=[cfg.get('iqiper-infra-root') + '/k8s/iqiper_services/modules/bouche/helm_values/bouche.yml'],
	set=['replicaCount=1', 'image.repository=bouche', 'config.master=false', 'config.slave=true']), allow_duplicates=True)
