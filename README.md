# `iqiper-infra`
## Introduction
This project aims to regroup everything that `iqiper` shall need in terms of
infrastructure.

There is 2 mains side to this repository.

### The OpenAPI

The `OpenApi` describe how our `REST` API works. It allows front-end and backend
developper use the same base to develop simultaneously.

The `REST` API is written in `YAML` using the `OpenApi 3.0` specification.

### The `kubernetes` cluster `IaC`

The term `IaC` means `Infrastructure As Code`. The more we write as code, the less
we rely on other's knowledge of the infrastructure. The ultimate goal beeing to
be able to mount the entire cluster using a single command.

The `kubernetes` cluster is configured and deployed using `terraform`, `helm` and
`kubectl`.

## Prerequisites

### `terraform`

[terraform](https://www.terraform.io/) binary version `0.13>=` or ulterior is required.

### `helm`

[helm](https://helm.sh/) is required to deploy most of our infrastructure.

### `swagger`

[swagger](https://helm.sh/) is required to parse, validate and build the `OpenApi` files.

### `redoc-cli`

[redoc-cli](https://github.com/Redocly/redoc/blob/master/cli/README.md) is used to visualize the `OpenApi`
in the browser.

### `minikube`

[minikube](https://kubernetes.io/en/docs/tasks/tools/install-minikube/) is used to develop locally, it allows to quickly spin up a `kubernetes`
cluster on one's machine.

### `tilt`

[tilt](https://tilt.dev/) is used to develop locally, it allow to quickly update and deploy onto a
`kubernetes` cluster an application beeing actively developed on one's machine.

## How to...

### ...build the `OpenAPI`

Using `swagger` you can run the following : 

```sh
swagger-cli bundle -r api/iqiper.yml > api/iqiper.json
```

### ...visualize the `OpenAPI` in the browser

Using `redoc-cli` you can run the following : 

```sh
redoc-cli serve -w api/iqiper.json
```

### ...deploy the `kubernetes` cluster

Using the run script in the `k8s` directory, you can deploy the cluster. 

The deployment occurs in 2 phases:

1. Deploying the base cluster. (pods, certificates, secrets, init scripts, etc...)
2. Configuring the runnning softwares (`Keycloak`, etc...)

Using the `k8s/run.sh` you can use the options `-1` and `-2` to deploy respectively
the different phases.

### ...run `tilt` to develop locally

After having started a `minikube` instance with the following command:

```sh
minikube start --cni calico --addons registry --addons metallb --disk-size=30G --driver=kvm2 --cpus=4 --memory 8G
```

and having deployed the `kubernetes` cluster using the `k8s/run.sh` script, you can
run the following to develop using `tilt`:

```sh
eval $(minikube docker-env)
tilt up --hud=true
```

or

```sh
./run.sh
```
